#!/bin/sh

time=$(TZ=UTC-9 date '+%F %R')

# 生成されたものをコミット&プッシュ
git pull origin master
git add -A
git status
git commit -m "Graph Update: ${time}"
git push
