import os
import sys
import re
import datetime
import urllib.request

dest_data = [
['The_Asset_Editors', '2021/04/05', 118165], ['Additional_Information', '2021/06/14', 189308], 
['DnD', '2021/08/26', 271310], ['Others', '2021/11/15', 349189], ['翻訳数 (ノルマ)', '2022/12/05', 'max_words']
]

log_url = os.environ.get("GRAPH_URL")
DAYS_MERGIN = 2

def main():
    req = urllib.request.Request(log_url)
    req_data = urllib.request.urlopen(req).read().decode('cp932')
    lines = req_data.splitlines(False)

#    log_path = './update_stats_graph.log'
#    with open(log_path, "r", encoding='utf_8') as f:
#        lines = f.read().splitlines(False)
    log_path_output = './graph.log'

    lines.pop(1)

    cat_length = len(lines[0].split('\t'))
    max_words = lines[1].split('\t')[5]

    last_time = lines[-1].split('\t')[0]
    last_words = lines[-1].split('\t')[6]

    lines[-1] += '\t' * (cat_length - lines[-1].count('\t') - 1) + ('\t' + last_words) * len(dest_data)

#    ls = [int(d) for d in re.split(r'[/ :]', last_time)]
#    ld = str(datetime.datetime(ls[0], ls[1], ls[2]) + datetime.timedelta(days=-1))
#    ld = re.sub(r'(^[^ ]+).*', r'\1', str(ld))
#    ld = ld.replace('-', '/')
#    lines.append(ld + '\t0' * (cat_length + len(dest_data) - 1))

    pre_words = 0
    inserted = []
    for idx, data in enumerate(dest_data):
        if data[2] == 'max_words':
            data[2] = max_words

        lines[0] += '\t' + data[0]
        d = data[1] + '\t' * cat_length
        for saved in inserted:
            d += '{0}\t'.format(saved)
        else:
            d += str(data[2])

        d += ('\t' + str(data[2])) * (len(dest_data) - (1 + idx))

        inserted.append(data[2])

        lines.insert(1, d)

    ls = [int(d) for d in re.split(r'[/ :]', dest_data[-1][1])]
    ld = datetime.datetime(ls[0], ls[1], ls[2])

    if DAYS_MERGIN > 0:
        dt_max = ld + datetime.timedelta(days=DAYS_MERGIN)
    else:
        dt_max = ld + datetime.timedelta(days=1)
    line_max_time = dt_max.strftime('%Y/%m/%d')
    lines.insert(1, line_max_time)

    with open(log_path_output, "w+", encoding='utf_8') as f:
        f.write('\n'.join(lines))

    return

if __name__ == '__main__':
    main()
    print('complete')
